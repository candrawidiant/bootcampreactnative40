//1
for (i=0; i<10; i++){
    console.log(i);
}


//2
for (x=1; x<10; x+=2){
    console.log(x);
}


//3
for (x=0; x<10; x+=2){
    console.log(x);
}


//4
let array1 = [1,2,3,4,5,6]
console.log(array1[5])


//5
let array2 = [5,2,4,1,3,5]
array2.sort()
console.log(array2)


//6
let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]
for(var i=0; i<array3.length; i++)
console.log(array3[i])


//7
let array4 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10]
for(var i=1; i<array4.length; i+=2)
console.log(array4[i])


//8
let kalimat= ["saya", "sangat", "senang", "belajar", "javascript"]
var slug = kalimat.join(" ");
console.log(slug)


//9
var sayuran=[]
sayuran.push('Kangkung','Bayam','Buncis','Kubis','Timun','Seledri','Tauge')
console.log(sayuran)



